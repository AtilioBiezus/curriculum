import './CreateCurriculum.css'

const CreateCurriculum = ({onButtonClick}) => {
    return (
        <div>
            <input type="button" value="Criar Curriculo" className="button-create" onClick={()=>onButtonClick()} />
        </div>
    )
}

export default CreateCurriculum
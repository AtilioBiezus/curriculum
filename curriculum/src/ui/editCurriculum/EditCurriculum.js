import React from "react";
import './EditCurriculum.css';

class EditCurriculum extends React.Component {

    constructor() {
        super()
        this.state = {
            name: '',
            email: '',
            phoneNumber: '',
            job: '',
            bio: '',
            validation: {
                form: false,
                name: null,
                email: null,
                phoneNumber: null,
                job: null,
                bio: null
            }
        }

        this.formRef = React.createRef()
        this.nameRef = React.createRef()
        this.emailRef = React.createRef()
        this.phoneNumberRef = React.createRef()
        this.jobRef = React.createRef()
        this.bioRef = React.createRef()
 

    }

    handleInput(event, fieldRef) {
        let fieldName = event.target.name

        let currentValidation = this.state.validation
        currentValidation.form = this.formRef.current.checkValidity()
        currentValidation[fieldName] = fieldRef.current.validity

        this.setState({
            [fieldName]: event.target.value,
            validation: currentValidation
        })
    }

    onSave() {
        localStorage.setItem('name', JSON.stringify(this.state.name));
        localStorage.setItem('email', JSON.stringify(this.state.email));
        localStorage.setItem('phoneNumber', JSON.stringify(this.state.phoneNumber));
        localStorage.setItem('job', JSON.stringify(this.state.job));
        localStorage.setItem('bio', JSON.stringify(this.state.bio));

        const { onButtonClick } = this.props;
        onButtonClick();
    }

    isFormEmpty() {
        return !this.state.name && !this.state.email && !this.state.phoneNumber && !this.state.job && !this.state.bio;
    }

    render() {
        return (
            <form ref={this.formRef} noValidate className="container-wrapper">

                <input type="text" name="name" value={this.state.name} ref={this.nameRef}
                    required onChange={event => this.handleInput(event, this.nameRef)} placeholder="Nome" />

                {
                    this.state.validation.name &&
                    !this.state.validation.name.valid &&
                    <div className="error-message">Nome é obrigatório</div>
                }

                <br />

                <input type="email" name="email" value={this.state.email} ref={this.emailRef}
                    required onChange={event => this.handleInput(event, this.emailRef)} placeholder="E-mail" />

                {
                    this.state.validation.email &&
                    this.state.validation.email.valueMissing &&
                    <div className="error-message">E-mail é obrigatório</div>
                }

                {
                    this.state.validation.email &&
                    !this.state.validation.email.valueMissing &&
                    !this.state.validation.email.valid &&
                    <div className="error-message">E-mail inválido</div>
                }

                <br />   

                <input type="number" name="phoneNumber" value={this.state.phoneNumber} ref={this.phoneNumberRef}
                    required onChange={event => this.handleInput(event, this.phoneNumberRef)} placeholder="Telefone" />

                {
                    this.state.validation.phoneNumber &&
                    !this.state.validation.phoneNumber.valid &&
                    <div className="error-message">Telefone é obrigatório</div>
                }

                <br />

                <input type="text" name="job" value={this.state.job} ref={this.jobRef}
                    required onChange={event => this.handleInput(event, this.jobRef)} placeholder="Cargo" />

                {
                    this.state.validation.job &&
                    !this.state.validation.job.valid &&
                    <div className="error-message">Cargo é obrigatório</div>
                }

                <br />

                <textarea type="text" name="bio" rows="5" value={this.state.bio} ref={this.bioRef}
                    required onChange={event => this.handleInput(event, this.bioRef)} placeholder="Histórico"  />

                {
                    this.state.validation.bio &&
                    !this.state.validation.bio.valid &&
                    <div className="error-message">Histórico é obrigatório</div>
                }

                <br />

                <input type="button" value="Salvar" className="button" disabled={this.isFormEmpty()} onClick={() => this.onSave()}/>

            </form>
        )
    }

}

export default EditCurriculum
import React from "react";
import './ShowCurriculum.css';

class ShowCurriculum extends React.Component{    
    render() {

        const { onButtonClick } = this.props; 

        return (
            <>
                <div className="container-name">
                    <h1>{JSON.parse(localStorage.getItem('name'))}</h1>
                </div>

                <div className="container-info">
                    <p>{JSON.parse(localStorage.getItem('email'))}</p>   
                    <p>{JSON.parse(localStorage.getItem('phoneNumber'))}</p>
                </div>

                <div className="container-info">
                    <p>{JSON.parse(localStorage.getItem('job'))}</p>
                </div>

                <div className="container-info">
                    <p>
                        {
                            JSON.parse(localStorage.getItem('bio'))
                        }
                    </p>
                </div>
                <input type="button" value="Editar" onClick={() => onButtonClick()} className="button"></input>
            </>
            
        )
    }
}

export default ShowCurriculum;
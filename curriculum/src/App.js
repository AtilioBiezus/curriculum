import React from "react";
import './App.css';

import CreateCurriculum from "./ui/createCurriculum/CreateCurriculum";
import EditCurriculum from "./ui/editCurriculum/EditCurriculum";
import ShowCurriculum from "./ui/showCurriculum/ShowCurriculum";

class App extends React.Component {

    avaiableScreens = {
        CREATE_CURRICULUM: 'create_curriculum',
        EDIT_CURRICULUM: 'edit_curriculum',
        SHOW_CURRICULUM: 'show_curriculum'
    }

    constructor() {
        super()
        this.state = {
            currentScreen: !localStorage.getItem('name') ? this.avaiableScreens.CREATE_CURRICULUM : this.avaiableScreens.SHOW_CURRICULUM
        }
    }

    switchSreen(screen) {
        this.setState({
            currentScreen: screen
        })
    }

    renderScreen() {
        switch (this.state.currentScreen) {
            case this.avaiableScreens.CREATE_CURRICULUM:
                return <CreateCurriculum onButtonClick={() => this.switchSreen(this.avaiableScreens.EDIT_CURRICULUM)} />

            case this.avaiableScreens.EDIT_CURRICULUM:
                return <EditCurriculum onButtonClick={() => this.switchSreen(this.avaiableScreens.SHOW_CURRICULUM)} />

            case this.avaiableScreens.SHOW_CURRICULUM:
                 return <ShowCurriculum onButtonClick={() => this.switchSreen(this.avaiableScreens.EDIT_CURRICULUM)} />

            default:
                break;
        }
    }

    render () {
        return (
            <>
                {
                    this.renderScreen()
                }
            </>
        );
    }
}

export default App;
